FROM adoptopenjdk:11
WORKDIR /app
COPY build/libs/*.jar hello-spring-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "hello-spring-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
#CMD java -jar hello-spring-0.0.1-SNAPSHOT.jar